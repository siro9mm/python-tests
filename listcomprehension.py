#examples of flat inversion of loops
mylist = [letter for letter in 'word']
print(mylist)

mynumbers = [x for x in range(0,11)]
print(mynumbers)

mynumbers = [x**2 for x in range(0,11) if x%2==0]
print(mynumbers)


celsius = [10, 20, 24.5, 35, 40]
fahrenheit = [( (9/5)*temp + 32 ) for temp in celsius ]
print(fahrenheit)

#the same concept but not a flat loop construction
fahrenheit = []
for temp in celsius:
    fahrenheit.append( (9/5)*temp + 32 )

print(fahrenheit)


#test two loops
mylist = []
for x in [2,4,6]:
    for y in [100,200,300]:
        mylist.append(x*y)

print(mylist)

