# my_file = open('samplefile.txt')

# contents = my_file.read()
# print(f'file contents: {contents}')
# my_file.seek(0)
# contents = my_file.read()
# print(f'file contents: {contents}')
# my_file.seek(0)
# contents_lines = my_file.read()
# print(f'file contents: {contents_lines}')

# my_file.close()

with open('samplefile.txt', mode='r') as my_file:
    contents = my_file.read()
    my_file.seek(0)
    contents_lines = my_file.readlines()
    print(f'file contents: {contents}')
    print(f'file contents: {contents_lines}')

with open('samplefile.txt', mode='a') as my_file:
    my_file.write('\nthis is a new line')

with open('samplefile.txt', mode='r') as my_file:
    print(my_file.read())

with open('samplefile2.txt', mode='w') as f:
    f.write('the program has created this file.')

with open('samplefile2.txt', mode='r') as f:
    print(f.read())


