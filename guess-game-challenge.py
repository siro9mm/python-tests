# Let's use while loops to create a guessing game.
# The Challenge:
# Write a program that picks a random integer from 1 to 100, and has players guess the number. The rules are:
# If a player's guess is less than 1 or greater than 100, say "OUT OF BOUNDS"
# On a player's first turn, if their guess is
# within 10 of the number, return "WARM!"
# further than 10 away from the number, return "COLD!"
# On all subsequent turns, if a guess is 
# closer to the number than the previous guess return "WARMER!"
# farther from the number than the previous guess, return "COLDER!"
# When the player's guess equals the number, tell them they've guessed correctly and how many guesses it took!

from random import randint

max_number = 100
min_number = 1

print(f'picking a random number between {max_number} and {min_number}...')
actual = randint(1, 100)

attempts = 0
guess = 0
difference = 100

while True:
    guess = abs(int(input("guess the number: ")))
    if guess < 1 or guess > 100:
        print('OUT OF BOUNDS!')
        continue
    else:
        attempts += 1

        if guess == actual:
            break

        # if guess <= (actual + 10) and guess >= (actual - 10):
        if abs(actual - guess) <= 10:

            if attempts == 1:
                print('WARM!')
            else:
                if abs(guess - actual) <= difference:
                    print('WARMER!')
                else:
                    print('COLDER!')

            difference = abs(guess - actual)
        else:
            print('COLD!')
    
print(f'congrats! you guessed {actual} in {attempts} attempt/s')


    
    




