nba_teams_lookup = {'celtics':'boston','lakers':'los angeles','sixers':'philadelphia'}
print('team lakers is located in {}'.format(nba_teams_lookup['lakers']))
print('team celtics is located in {}'.format(nba_teams_lookup['celtics']))
print('team sixers is located in {}'.format(nba_teams_lookup['sixers']))

print('favorite location is {}'.format(nba_teams_lookup['lakers'].upper()))

my_data = {'k1':123,'k2':789,'k3':{'insidekey':100}}
print('inside key data is {}'.format(my_data['k3']['insidekey']))

insidekey_value = my_data['k3']['insidekey']
print(f'inside key data is {insidekey_value}')

my_favorite_artists = {'grunge':['nirvana','stp','alice in chains'],'metal':['metallica','megadeth']}
my_favorite_artists['grunge'].sort()
my_favorite_artists['metal'].sort()
print("my favorite grunge artists are {}".format(my_favorite_artists['grunge']))
print("my favorite grunge artists are {}".format(my_favorite_artists['metal']))

my_favorite_artists['jazz'] = ['john coltrane','miles davis']
print('my favorite artists are {}'.format(my_favorite_artists.values()))
print('my favorite music genre are {}'.format(my_favorite_artists.keys()))
print('all artists and genre: {}'.format(my_favorite_artists.items()))

Capitals = {'Russia': 'Moscow', 'Ukraine':'Kiev', 'USA': 'Washington'}
print(Capitals)
Capitals = dict(Russia = 'Moscow', Ukraine = 'Kiev', USA = 'Washington')
print(Capitals)
Capitals = dict([("Russia", "Moscow"), ("Ukraine", "Kiev"), ("USA", "Washington")])
print(Capitals)
Capitals = dict(zip(["Russia", "Ukraine", "USA"], ["Moscow", "Kiev", "Washington"]))
print(Capitals)
