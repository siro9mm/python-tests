import math


class Cyclinder:

    def __init__(self, height=1, radius=1):
        self.height = height
        self.radius = radius

    def volume(self):
        r = self.radius
        h = self.height
        return math.pi * r ** 2 * h

    def surface_area(self):
        r = self.radius
        h = self.height
        return (math.pi * 2 * r * h) + (math.pi * 2 * r ** 2)


def main():
    print('OOP HW 64')
    cy = Cyclinder(2, 3)
    print(cy.volume())
    print(cy.surface_area())


if __name__ == "__main__":
    main()
    pass
