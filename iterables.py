my_iterable_list = [1,2,3,"asdfg"]
for item_name in my_iterable_list:
    print(item_name)

my_iter_tuples = [(1,2),(3,4),(5,6)]
for item_name in my_iter_tuples:
    print(item_name)

# tuple unpacking
for a,b in my_iter_tuples:
    print(a)
    # print(b)

# dictionary unpacking
my_dict = {'k1':1, 'k2':2, 'k3':3}
for key,value in my_dict.items():
    print("key,value: {},{}".format(key, value))

