from packagingtest.MyMainPackage import some_main_script
from packagingtest.MyMainPackage.SubPackage import mysubscript


def main():
    some_main_script.report_main()
    mysubscript.sub_report()


if __name__ == "__main__":
    main()
