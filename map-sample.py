def square(num):
    return num**2

def splicer(mystring):
    if len(mystring)%2 == 0:
        return 'EVEN'
    else:
        return mystring[0]


def main():
    print('-square-')
    my_nums = [1,2,3,4,5]
    for item in map(square, my_nums):
        print(item)
    print(list(map(square, my_nums)))

    print('-splicer-')
    names = ['Ryan', 'Dele', 'Syakolite']
    print(list(map(splicer, names)))


if __name__ == "__main__":
    main()