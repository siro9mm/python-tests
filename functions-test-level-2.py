def has_33(numbers):
    '''
    has_33([1, 3, 3]) → True
    has_33([1, 3, 1, 3]) → False
    has_33([3, 1, 3]) → False
    '''
    previous_number = 0
    has_33_ = False
    for number in numbers:
        if number == 3 and number == previous_number:
            has_33_ = True
        previous_number = number

    return has_33_

def has_33_improved(nums):
    for i in range(0, len(nums)-1):
      
        # nicer looking alternative in commented code
        #if nums[i] == 3 and nums[i+1] == 3:
    
        if nums[i:i+2] == [3,3]:
            return True  
    
    return False

def paper_doll(text):
    '''
    paper_doll('Hello') --> 'HHHeeellllllooo'
    paper_doll('Mississippi') --> 'MMMiiissssssiiippppppiii'
    '''
    new_text = ''
    for letter in text:
        for x in range(3):   # How to create a loop without this warning? 
            new_text+=letter

    return new_text

def paper_doll_improved(text):
    result = ''
    for char in text:
        result += char * 3
    return result

def blackjack(a, b, c):
    '''
    blackjack(5,6,7) --> 18
    blackjack(9,9,9) --> 'BUST'
    blackjack(9,9,11) --> 19
    '''
    result = ''
    if (a+b+c) <= 21:
        result = str(a + b + c)
    else:
        if a == 11 or b == 11 or c == 11:
            result = str(a + b + c - 10)
        else:
            result = 'BUST'
    
    return result

def blackjack_improved(a,b,c):
    
    if sum((a,b,c)) <= 21:
        return sum((a,b,c))
    elif sum((a,b,c)) <=31 and 11 in (a,b,c):
        return sum((a,b,c)) - 10
    else:
        return 'BUST'

def summer_69(arr):
    '''
    summer_69([1, 3, 5]) --> 9
    summer_69([4, 5, 6, 7, 8, 9]) --> 9
    summer_69([2, 1, 6, 9, 11]) --> 14
    '''
    total = 0
    if len(arr) == 0:
        return total

    do_sum = True

    for num in arr:
        if num == 6:
            do_sum = False
        if do_sum == True:
            total += num
        if num == 9:
            do_sum = True

    return total

def summer_69_improved(arr):
    total = 0
    add = True
    for num in arr:
        while add:
            if num != 6:
                total += num
                break
            else:
                add = False
        while not add:
            if num != 9:
                break
            else:
                add = True
                break
    return total
    

def main():
    print('-------')
    print(has_33([1, 3, 3]))
    print(has_33([1, 3, 1, 3]))
    print(has_33([3, 1, 3]))
    print('-------')
    print(paper_doll('Hello'))
    print(paper_doll('Mississippi'))
    print('-------')
    print(blackjack(5, 6, 7))
    print(blackjack(9, 9, 9))
    print(blackjack(9, 9, 11))
    print('-------')
    print(summer_69([1, 3, 5]))
    print(summer_69([4, 5, 6, 7, 8, 9]))
    print(summer_69([2, 1, 6, 9, 11]))
    print(summer_69([2, 9, 6, 9, 11]))
    print(summer_69([]))

if __name__ == "__main__":
    main()
    
