class Book:

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return f"{self.title} by {self.author}"

    def __len__(self):
        return self.pages

    def __del__(self):
        return print("book deleted")


def main():
    b = Book('Hardy Boys', 'Mr Ryan', 500)
    print(b)
    print(str(b))
    print(len(b))
    del b


if __name__ == "__main__":
    main()
    pass
