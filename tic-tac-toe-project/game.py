from IPython.display import clear_output
import random


def display_board(board):
    """
    This function prints the board
    :param board: <List> The board that contains the markers
    :return: None
    """
    print(board[7] + '|' + board[8] + '|' + board[9])
    print(board[4] + '|' + board[5] + '|' + board[6])
    print(board[1] + '|' + board[2] + '|' + board[3])


def player_input():
    """
    This function decides player marker and output as tuple
    :return: <Tuple> (Player 1 marker, Player 2 marker)
    """
    marker = ''
    while marker != 'X' and marker != 'O':
        marker = input('Enter player 1 marker. Choose X or O: ').upper()

    if marker == 'X':
        return ('X','O')
    else:
        return ('O','X')


def place_marker(board, marker, position):
    """
    This function places the marker on the board on the position chosen
    :param board: <List> The board that contains the markers
    :param mark: <str> The marker that need to check if winning combination
    :param position: <int> The board position
    :return: None
    """
    board[position] = marker


def check_win(board, mark):
    """
    This function checks if the board contains a winning combination
    :param board: <List> The board that contains the markers
    :param mark: <str> The marker that need to check if winning combination
    :return: <Boolean> Returns True if board contains winning combination. Otherwise, returns False
    """
    return ((board[7] == board[8] == board[9] == mark) or  # across the top
            (board[4] == board[5] == board[6] == mark) or  # across the middle
            (board[1] == board[2] == board[3] == mark) or  # across the bottom
            (board[7] == board[4] == board[1] == mark) or  # column 1
            (board[8] == board[5] == board[2] == mark) or  # column 2
            (board[9] == board[6] == board[3] == mark) or  # column 3
            (board[7] == board[5] == board[3] == mark) or  # diagonal 1
            (board[9] == board[5] == board[1] == mark))  # diagonal 2


def choose_first():
    """
    Flips a random number (0 and 1) to determine which player moves first
    :return: <str> Returns a string value of the player
    """
    flip = random.randint(0, 1)
    if flip == 0:
        return 'Player 1'
    else:
        return 'Player 2'


def check_space(board, position):
    """
    This function checks if marker position in the board is empty
    :param board: <List> The board that contains the markers
    :param position: <int> The position that need to verify
    :return: <Boolean> Returns True if board position is empty. Otherwise, returns False
    """
    return (board[position] == ' ')


def check_full(board):
    """
    This function checks if all marker positions in the board is filled
    :param board: <List> The board that contains the markers
    :return: <Boolean> Returns True if board is full
    """
    for i in range(1, 10):
        if check_space(board, i):
            return False

    return True

def player_choice(board):
    """
    This function asks for player next position (1-9) and then uses check_space function to check if position is free
    :param board: <List> The board that contains the markers
    :return: <int> Returns integer value of the position chosen
    """
    position = 0
    while position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not check_space(board, position):
        position = int(input('Enter position {1-9}: '))
        # TODO: Need to restrict valid input range here!

    return position


def replay():
    """
    This function asks player if he wants to play again
    :return: <Boolean> Returns True if play again
    """
    choice = input('Play again (y/n)?')
    return choice.upper() == "Y"


def main():
    print('Welcome to my tic tac toe game!')

    while True:
        game_board = [' '] * 10
        player1_marker, player2_marker = player_input()
        turn = choose_first()
        print(turn + ' choose first!')

        play_game = input('Ready to play (y/n)?')
        if play_game.lower() == 'y':
            game_on = True
        else:
            game_on = False

        while game_on:
            if turn == 'Player 1':
                display_board(game_board)
                position = player_choice(game_board)
                place_marker(game_board, player1_marker, position)

                if check_win(game_board, player1_marker):
                    display_board(game_board)
                    print('Player 1 wins!')
                    game_on = False
                else:
                    if check_full(game_board):
                        display_board(game_board)
                        print('Game tied! No one wins!')
                        game_on = False
                    else:
                        turn = 'Player 2'

            else:
                display_board(game_board)
                position = player_choice(game_board)
                place_marker(game_board, player2_marker, position)

                if check_win(game_board, player2_marker):
                    display_board(game_board)
                    print('Player 2 wins!')
                    game_on = False
                else:
                    if check_full(game_board):
                        display_board(game_board)
                        print('Game tied! No one wins!')
                        game_on = False
                    else:
                        turn = 'Player 1'

        if not replay():
            break


if __name__ == "__main__":
    main()
    pass
