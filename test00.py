# Numbers:
# number data types including floating point values

# Strings:
# string and character data types

# Lists:
# unordered collection of string and number data types

# Tuples:
# mutable key value pairs of string,number,list data types

# Dictionaries:
# immutable key value pairs of string,number,list data types

# Write an equation that uses multiplication, division, an exponent, addition, and subtraction that is equal to 100.25.
my_formula = 10 ** 2 * 5 / 4 - 25 + 0.25
print(my_formula)

# Answer these 3 questions without typing code. Then type code to check your answer.

# What is the value of the expression 4 * (6 + 5)
# 120
# What is the value of the expression 4 * 6 + 5
# 24/5
# What is the value of the expression 4 + 6 * 5
# 34

# What is the type of the result of the expression 3 + 1.5 + 4?
# floating point type

# What would you use to find a number’s square root, as well as its square?
# ??

s = 'hello'
# Print out 'e' using indexing
print(s[1])
s ='hello'
# Reverse the string using slicing
print(s[::-1])

s ='hello'
# Print out the 'o'
print(s[-1])
print(s[4])

# Build this list [0,0,0] two separate ways.

my_list = [0,0,0]
print(my_list)
my_list = [0]
print(my_list)

# Reassign 'hello' in this nested list to say 'goodbye' instead:
list3 = [1,2,[3,4,'hello']]
list3[2][2] = 'goodbye'
print(list3)

# Sort the list below:
list4 = [5,3,4,6,1]
list4.sort()
print(list4)

# Using keys and indexing, grab the 'hello' from the following dictionaries:
d = {'simple_key':'hello'}
# Grab 'hello'
print(d['simple_key'])
d = {'k1':{'k2':'hello'}}
# Grab 'hello'
print(d['k1']['k2'])
# Getting a little tricker
d = {'k1':[{'nest_key':['this is deep',['hello']]}]}
#Grab hello
my_hello = d['k1'][0]['nest_key'][1][0]
print(my_hello)

# This will be hard and annoying!
d = {'k1':[1,2,{'k2':['this is tricky',{'tough':[1,2,['hello']]}]}]}
my_hello = d['k1'][2]['k2'][1]['tough'][2][0]
print(my_hello)

# Can you sort a dictionary? Why or why not?
# Dictionaries cannot be sorted because it is ordered using key

# What is the major difference between tuples and lists?
# tuples use parenthesis (immutable collecton)
# lists use brackets (mutable collection)

# How do you create a tuple?
my_tuple = (1,2)
print(my_tuple)

# What is unique about a set?
# a set only contains unique entries in a collection

# Use a set to find the unique values of the list below:
list5 = [1,2,2,33,4,4,11,22,3,3,2]
#cast list to a set!!!!
print(set(list5))


# What will be the resulting Boolean of the following pieces of code (answer fist then check by typing it in!)

# In [ ]:
# Answer before running cell
# 2 > 3'
# false
# In [ ]:
# Answer before running cell
# 3 <= 2
# false
# In [ ]:
# Answer before running cell
# 3 == 2.0
# false
# In [ ]:
# Answer before running cell
# 3.0 == 3
# true
# In [ ]:
# Answer before running cell
# 4**0.5 != 2
# true


# Final Question: What is the boolean output of the cell block below?
# two nested lists
l_one = [1,2,[3,4]]
l_two = [1,2,{'k1':4}]
# True or False?
l_one[2][0] >= l_two[2]['k1']
# false. 3 is lesser than 4!



























