# spy game 007 test
# write a function that takes in a list of integers and returns True if it contains 007
def spygame(nums):
    
    code = [0, 0, 7, 'z']

    for num in nums:
        if num == code[0]:
            code.pop()

    return len(code) == 1

#count the number of primes in a given number
def count_primes(num):
    if num <= 2:
        return 0
    
    primes = [2]
    x = 3

    while x <= num:
        for y in range(3, x, 2):
            print(f'x: {x}, y: {y}')
            if x%y == 0:
                x += 2
                break
        else:  # why do we use else in a for loop here?!!!!
            primes.append(x)
            print(f'primes: {primes}')
            x += 2

    print(primes)
    return len(primes)

def main():
    test_numbers = [9,0,9,5,0,1,2,7,0]
    print(spygame(test_numbers))
    test_numbers = [9,0,0,5,0,1,2,7,0] #subject for improvement here! 3 leading zeroes destroys the pop algorithm!
    print(spygame(test_numbers))
    test_numbers = [1,0,9,5,2,1,2,7,0]
    print(spygame(test_numbers))

    print('---------------------')
    print(count_primes(100))

if __name__ == "__main__":
    main()
    pass