my_tuple = (1,2,3)
print(type(my_tuple))

my_set = [1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3]
print(type(my_set))

# cast set to list to get non repeated members
print(set(my_set))
