my_string = 'test string'

for letter in my_string:
    if letter == 't':
        continue
    print(letter)

print('-------')
for letter in my_string:
    pass

print('-------')
for letter in my_string:
    print(letter)
    if letter == 's':
        break


