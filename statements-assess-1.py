# use for, split, and if to create a statement that will printout 
# words that start with 's' 

st = 'Print only the words that start with s in this Sentence'

my_list =  st.split()
my_statement = ''
for word in my_list:
    for letter in word:
        if letter[0] == 's':
            my_statement = my_statement + word + ' ' 
        break

print(my_statement)
# best solution:
for word in st.split():
    if word[0].lower() == 's':
        print(word)
print('------')

# Use range() to print all the even numbers from 0 to 10.
even_numbers = list(range(0, 11, 2))
print(even_numbers)
print('------')

# Use a List Comprehension to create a list of all numbers between 1 and 50 that are divisible by 3.
filtered_numbers = [x for x in range(1, 51) if x%3 == 0]
print(filtered_numbers)

filtered_numbers = []
for x in range(1, 51):
    if x%3 == 0:
        filtered_numbers.append(x)

print(filtered_numbers)
print('------')

# Go through the string below and if the length of a word is even print "even!"
st = 'Print every word in this sentence that has an even number of letters'
word_list = st.split()
for word in word_list:
    word_length = len(word)
    if word_length%2 == 0:
        print(word)

# best solution
for word in st.split():
    if len(word)%2 == 0:
        print(word+ ' is even')
        
print('------')

# Write a program that prints the integers from 1 to 100. 
# But for multiples of three print "Fizz" instead of the number, 
# and for the multiples of five print "Buzz". 
# For numbers which are multiples of both three and five print "FizzBuzz".

my_numbers = list(range(1, 101))
index = 0
for num in my_numbers:
    if num % 3 == 0 and num % 5 == 0:
        my_numbers[index] = 'FizzBuzz'
    elif num % 5 == 0:
        my_numbers[index] = 'Buzz'
    elif num % 3 == 0:
        my_numbers[index] = 'Fizz'
    index += 1

print(my_numbers)
print('------')

# Use List Comprehension to create a list of the first letters of every word in the string below:
st = 'Create a list of the first letters of every word in this string'
print(st)
words_list = st.split()
first_letters = [word[0] for word in words_list]
print(first_letters)

# END