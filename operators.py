print('range!!')
#range operator
for num in range(0,11,2):
    print(num)

print('------')

x = 3
for y in range(3, 5, 2):
        print(y)
        x += 2

print('------')
#range casted with list operator
my_list = list(range(0, 11, 2))
for item in my_list:
    print(item)

print('------')

#enumerate operator
my_word = 'abcde'
for item in enumerate(my_word):
    print(item)

print('------')
print('enumerate!')
for x,y in enumerate(my_word):
    print(f'index={x},letter={y}')
    print('index={},letter={}'.format(x, y))

print('------')

#zip operator
my_list1 = [1,2,3,4,5,6,7,8] #long list compared to other lists
my_list2 = ['a','b','c']
my_list3 = [100, 200, 300]
my_archive = zip(my_list1, my_list2, my_list3)

for item in my_archive:
    print(item)

for x, y, z in zip(my_list1, my_list2, my_list3):
    print(f'{x}:{y}:{z}')
    
print('------')

my_archive_list = list(zip(my_list1, my_list2, my_list3))
print(my_archive_list)
for my_tuples in my_archive_list:
    print(my_tuples)
    #in operator
    if 200 in my_tuples:
        print('200 found!')

print('------')

#import operator
from random import shuffle
print(my_list1)
shuffle(my_list1)
print(my_list1)

from random import randint
my_int = randint(0, 100)
print(my_int)

print('------')
#input operator
your_answer = input('what is your name? ')
print(f'your name is : {your_answer}')
#note:  cast int to your_age to convert to int type from string
your_age = int(input('what is your age? '))
print('your real age is : {}'.format(your_age + 100))











