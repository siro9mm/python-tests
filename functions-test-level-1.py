def lesser_of_two_evens(a, b):
    if a%2 == 0 and b%2 == 0:
        if a < b:
            return a
        else:
            return b
    else:
        if a > b:
            return a
        else:
            return b

def lesser_of_two_evens_improved(a, b):
    if a%2 == 0 and b%2 == 0:
        return min(a, b)
    else:
        return max(a, b)

def animal_crackers(text):
    word_list = text.split()
    word1 = word_list[0]
    word2 = word_list[1]
    if word1[0] == word2[0]:
        return True
    else:
        return False
    
def animal_crackers_improved(text):
    wordlist = text.split()
    return wordlist[0][0] == wordlist[1][0]

def makes_twenty(n1, n2):
    '''
    makes_twenty(20,10) --> True
    makes_twenty(12,8) --> True
    makes_twenty(2,3) --> False
    '''
    if n1 == 20 or (n1 + n2) == 20:
        return True
    else:
        return False

def makes_twenty_improved(n1,n2):
    return (n1+n2)==20 or n1==20 or n2==20

def old_macdonald(name):
    '''
    old_macdonald('macdonald') --> MacDonald
    '''
    new_name = ''
    index = 0
    for letter in name:
        if index == 0 or index == 3:
            new_name += letter.upper()
        else:
            new_name += letter
        index += 1
    
    return new_name

def old_macdonald_improved(name):
    if len(name) > 3:
        return name[:3].capitalize() + name[3:].capitalize()
    else:
        return 'Name is too short!'

def master_yoda(sentence):
    '''
    master_yoda('I am home') --> 'home am I'
    master_yoda('We are ready') --> 'ready are We'
    '''
    word_list = sentence.split()
    reverse_list = word_list[::-1] # reverse() does not work in functions call!
    reverse_sentence = " ".join(reverse_list)
    return reverse_sentence

def master_yoda_improved(text):
    return ' '.join(text.split()[::-1])

def main():
    print('----------------')
    print(lesser_of_two_evens(2, 5))
    print(lesser_of_two_evens(2, 4))
    print('----------------')
    print(animal_crackers("Lovely Llama"))
    print(animal_crackers("Great Lion"))
    print('----------------')
    print(makes_twenty(20, 3))
    print(makes_twenty(18, 2))
    print(makes_twenty(5, 3))
    print('----------------')
    print(old_macdonald('macdonald'))
    print('----------------')
    print(master_yoda('i am home'))
    print(master_yoda('we are ready'))

if __name__ == "__main__":
    main()
    




