x = 50

def func(x):
    print(f'X is {x}')

    # LOCAL
    x = 200
    print(f'I just locally changed X to {x}')

def func2():
    global x
    print(f'X is {x}')

    # LOCAL
    x = 'new value'
    print(f'I just locally changed global X to {x}')

def main():
    func(x)
    print(x)
    func2()
    print(x)

if __name__ == "__main__":
    main()