def test1():
    try:
        for i in ['a', 'b', 'c']:
            print(i ** 2)
    except TypeError:
        print('TypeError occurred!')


def test2():
    try:
        x = 5
        y = 0
        z = x / y
    except ZeroDivisionError:
        print('ZeroDivisionError occurred!')
    finally:
        print('All done')


def ask():
    while True:
        try:
            num = int(input('Input an integer: '))
        except:
            print('An error occurred! Please try again!`\n')
        else:
            break

    print('Thank you, your number squared is: {}'.format(num * num))


def main():
    test1()
    test2()
    ask()


if __name__ == "__main__":
    main()
    pass
