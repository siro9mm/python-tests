def maskify(cc):
    print(cc)
    if len(cc) < 6 or cc == "":
        return cc
    
    core_num = ""
    for num in cc:
        if not num == '-':
            core_num += num
            
    if core_num.isalpha():
        return cc

    digits_to_mask = cc[1:-4]
       
    masked = ""
    for num in digits_to_mask:
        if num == '-':
            masked += '-'
        if not num.isdigit():
            masked += num
        else:
            masked += '#'
    
    print(cc[0] + masked + cc[-4:])
    
    return cc[0] + masked + cc[-4:]
        

def main():
    print(maskify("A1234567BCDEFG89HI"))
    print(maskify("ABCD-EFGH-IJKLM-NOPQ"))

if __name__ == "__main__":
    main()
    pass