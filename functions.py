def pig_latin(word):
    first_letter = word[0]

    if first_letter in 'aeiou':
        pig_word = word + 'ay'
    else:
        pig_word = word[1:] + first_letter + 'ay'

    return pig_word

def myfunc(*args, **kwargs):
    print(args)
    print(kwargs)

def sumtotal(*args):
    return sum(args)

def myfunc2(*args):
    mylist = []
    print(args)
    for item in args:
        if item%2 == 0:
            mylist.append(item)

    return mylist

def myfunc3(word):
    mylist = []
    for letter in range(len(word)):
        if letter%2 == 0:
            mylist.append(word[letter].lower())
        else:
            mylist.append(word[letter].upper())
    return ''.join(mylist)


def main():
    print(pig_latin('word'))
    print(pig_latin('apple'))
    #args and kwargs test
    myfunc(1, 2, 3, 4, 10, 200, 300, food='eggs', fruit='apple')
    print(sumtotal(1,2,3,4,5,6,7,8))
    #myfunc2 test
    print(myfunc2(1,2,3,4,5,6))
    #myfunc3 test
    print(myfunc3('Anarchy'))

if __name__ == "__main__":
    main()
    pass
