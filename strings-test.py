print('This {2} {1} {0}'.format('game','the','is'))
print('This {i} {t} {g}'.format(g='game',t='the',i='is'))
result = 198/564
print("The result is {r:1.5f}".format(r=result))
name = "Ryan"
print(f'His name is {name}')
my_list = ['one','two','three']
print(my_list[-1])
my_list.append('nine')
print(my_list)
my_list.pop(0)
print(my_list)
my_list.pop()
print(my_list)
print(type(my_list))
new_list = ['a','x','g','s','o']
new_list.sort()
print(new_list)
new_list.reverse()
print(new_list)
my_nested_list = [1,2,[3,4]]
print(f'inner list members are {my_nested_list[2:]}')
print('inner list members are {}'.format(my_nested_list[2:]))


