class Account:

    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance

    def __str__(self):
        str1 = str("Account owner: {}".format(self.owner))
        str2 = str("Account balance: {}".format(self.balance))
        return str1 + "\n" + str2

    def deposit(self, amount):
        self.balance += amount
        print('Deposit accepted')

    def withdraw(self, amount):
        if self.balance >= amount:
            self.balance -= amount
            print('Withdrawal accepted')
        else:
            print('Funds unavailable!')


def main():
    print('OOP CHALLENGE 66')
    acct1 = Account('Ryan', 300)
    acct2 = Account('Polokotoy', 10)
    print(acct1)
    print(acct1.owner)
    print(acct1.balance)
    acct1.deposit(1000)
    acct1.deposit(2000)
    acct1.withdraw(6000)
    print(acct1)
    print(acct2)
    acct2.withdraw(20)
    print(acct2)


if __name__ == "__main__":
    main()
    pass
