def numbertoordinal(number):
    """
    Take a number and return it as a string with the correct ordinal indicator suffix (in English)
    :param number: <int> The number to be converted to a string ordinal
    :return: <str> Returns a string ordinal based off of the number
    """
    if number <= 0 and number <= 10000:
        return str(0)
        
    num_str = str(number)
    last_digit = num_str[-1:]
    print
    #last_digit = number % 10
    x = number%10
    print(x)

    ch = 'th'
    
    if last_digit == '1':
        if (number > 10 and number < 20):
            ch = 'th'
        else:
            ch = 'st'

    if last_digit == '2':
        if (number > 10 and number < 20):
            ch = 'th'
        else:
            ch = 'nd'
            
    if last_digit == '3':
        if (number > 10 and number < 20):
            ch = 'th'
        else:
            ch = 'rd'

    
    return num_str + ch

def main():
    print(numbertoordinal(110001))
    print(numbertoordinal(111))
    print(numbertoordinal(11))
    print(numbertoordinal(110002))
    print(numbertoordinal(1012))
    print(numbertoordinal(12))
    print(numbertoordinal(110003))
    print(numbertoordinal(1013))
    print(numbertoordinal(13))
    print(numbertoordinal(31))
    print(numbertoordinal(32))
    print(numbertoordinal(33))

if __name__ == "__main__":
    main()
    pass